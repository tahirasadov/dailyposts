<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Post;
use Faker\Generator as Faker;

$factory->define(Post::class, function (Faker $faker) {

    $categories = array(
        'tech'     => 'Tech',
        'sport'    => 'Sport',
        'politics' => 'Politics',
        'science'  => 'Science',
        'business' => 'Business',
    );

    shuffle($categories);
    $category = strtolower($categories[0]);
    return [
        'title'          => $faker->realText(60),
        'body'           => $faker->realText(250),
        'category'       => $category,
        'author'         => 1,
        'featured_image' => $faker->imageUrl(640, 480, 'sports'),
    ];
});
