<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;
use App\Post;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        


        factory(App\User::class, 50)->create()->each(function ($user) {
            $user->posts()->save(factory(App\Post::class)->make());
        });

        // print_r($users);
        $posts = factory(App\Post::class, 3)->make();
        // $post = new Post();
        // for($i = 1; $i < 13; $i++)
        // {
        //     $postData = factory(App\Post::class)->create();
        //     $post->save($postData);
        // }
            // echo "$i \n";
            // echo Faker::realText(350);
            // array(
            //     'title' => request('title'),
            //     'slug' => request('slug'),
            //     'body' => request('body'),
            //     'category' => 'tech',
            //     'author' => 1,
            //     'featured_image' => "$i.jpg",
            // )
            // $user->posts()->save(factory(App\Post::class)->make());
            // print(factory(App\Post::class)->make());
            // Post::create(array(
            //     'title' => request('title'),
            //     'slug' => request('slug'),
            //     'body' => request('body'),
            //     'category' => request('category'),
            //     'author' => auth()->id(),
            //     'featured_image' => $imageName,
            // ));
        // }
        //
    }
}
