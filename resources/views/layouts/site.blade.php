<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.min.css">
    <link href="https://fonts.googleapis.com/css?family=News+Cycle&display=swap" rel="stylesheet"> 
    <link rel="stylesheet" href="/css/site-style.css">
    <title>{{ config('app.name', 'Daily Posts - Local news') }}</title>
</head>
<body>
    <div class="section header">
        <div class="container">
            <div class="level">
                <div class="level-left">
                    @guest
                        <a href="/login">Log in</a>
                    @else
                        <a href="/posts">Administrator page</a>
                    @endif
                </div><!-- level-left -->
                <div class="level-right">
                    
                    {!! Form::open(['route' => 'search', 'method'=>'GET',  'role' => 'form']) !!}
                    <div class="field has-addons">
                        <div class="control">
                        {{ Form::text('search', '', ['class' => 'input']) }}
                        </div><!-- control -->
                        <div class="control">
                                {{ Form::button('Search', ['class' => 'button', 'type' => 'submit']) }}
                        </div><!-- control -->
                    </div><!-- field has-addons -->
                    {!! Form::close() !!}


                </div><!-- level-right -->
            </div><!-- level -->
        </div><!-- container -->

        <div class="container">
            <div class="level">
                <div class="level-item">
                    <a href="/"><img src="/img/logo.png" alt="Logo"></a>
                </div><!-- level-item -->
            </div><!-- level -->
        </div><!-- container -->

    </div><!-- section header -->
    <div class="section">
        <div class="container">
            @yield('content')
        </div><!-- container -->

    </div><!-- section -->

</body>
</html>