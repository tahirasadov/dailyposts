<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.min.css">
    <link rel="stylesheet" href="/css/style.css">
    
</head>
<body>
    @if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif
    <div class="section section-header">
        <div class="container">
            <div class="level">
                <div class="level-left">
                    <a href="/">Administrator panel</a>    
                </div><!-- level-left -->
                <div class="level-right">
                    <a href="/posts">Posts</a>    
                    <a href="/posts/create">New posts</a>
                    <a href="#">Logout</a>    
                </div><!-- level-right -->    
            </div><!-- level -->    
        </div><!-- container -->    
    </div><!-- section section-header -->

    <div class="section section-content">
        <div class="container">
            <div class="columns"> 
                <div class="column body">
                    @yield('content')
                </div><!-- column body -->    
            </div><!-- columns -->    
        </div><!-- container -->        
    </div><!-- section section-content -->

</body>
</html>