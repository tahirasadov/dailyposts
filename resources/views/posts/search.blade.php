@extends('layouts.site')

@section('content')
<h2 class="title">Search results</h2>

<div class="content">
    @foreach ($posts as $post)
    <div class="search-item">
        <a href="{{ $post->url() }}"><h4>{{ $post->title }}</h4></a>

        @if (strpos($post->featured_image, 'http') !== false)
            <img class="search-image" src="{{ $post->featured_image }}" alt="Thumbnail">
        @else
            <img class="search-image" src="/uploads/{{ $post->featured_image }}" alt="Thumbnail">
        @endif
        <div class="meta">
            <span class="publish-date">{{ date("d-m-Y", strtotime($post->created_at)) }}</span>
        </div><!-- meta -->
        {{ $post->body }}
    </div><!-- search-item -->
    <div class="clearfix"></div><!-- clearfix -->
    <hr>
    @endforeach
    </div><!-- content -->

@endsection