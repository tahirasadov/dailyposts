@extends('layouts.app')


@section('content')
<div class="py-4">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h1>Create new post</h1>
                {!! Form::open(['route'=>'posts.store', 'method'=>'STORE', 'files' => true, 'role' => 'form', 'novalidate' => '']) !!}
                <div class="form-group">
                    {{ Form::label('title', 'Title') }}
                    {{ Form::text('title', '', ['class' => ($errors->has('title')) ? 'form-control is-invalid' : 'form-control']) }}
                    <div class="invalid-feedback">
                        @if ($errors->has('title')) <p style="color:red;">{{ $errors->first('title') }}</p> @endif
                    </div>
                </div><!-- form-group -->

                <div class="form-group">
                    {{ Form::label('body', 'Body') }}
                    {{ Form::textarea('body', '', ['class' => ($errors->has('body')) ? 'form-control is-invalid' : 'form-control']) }}
                    <div class="invalid-feedback">
                        @if ($errors->has('body')) <p style="color:red;">{{ $errors->first('body') }}</p> @endif
                    </div>
                </div><!-- form-group -->

                <div class="form-group">
                    {{ Form::label('category', 'Category') }}
                    {{ Form::select('category', $categories, '', ['class' => ($errors->has('category')) ? 'form-control is-invalid' : 'form-control', 'placeholder' => 'Select category']) }}
                    <div class="invalid-feedback">
                        @if ($errors->has('category')) <p style="color:red;">{{ $errors->first('category') }}</p> @endif
                    </div>
                </div><!-- form-group -->
                
                <div class="form-group">
                    {{ Form::label('featured_image', 'Featured image') }}
                    {{ Form::file('featured_image', ['class' => ($errors->has('featured_image')) ? 'form-control is-invalid' : 'form-control']) }}
                    <div class="invalid-feedback">
                        @if ($errors->has('featured_image')) <p style="color:red;">{{ $errors->first('featured_image') }}</p> @endif
                    </div>
                </div><!-- form-group -->

                <div class="form-group">
                    {{ Form::submit('Submit', ['class' => 'btn btn-primary mb-2']) }}
                </div><!-- form-group -->

                {!! Form::close() !!}
            </div><!-- col-md-8 -->
        </div><!-- row justify-content-center -->
    </div><!-- container -->
</div><!-- py-4 -->

@endsection