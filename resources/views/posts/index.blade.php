@extends('layouts.app')

@section('content')

<div class="py-4">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <h1>Posts</h1>
                    <table class="table">
                        <thead>
                            <tr>
                            <th scope="col">Image</th>
                            <th scope="col">Title</th>
                            <th scope="col">Category</th>
                            <th scope="col">Author</th>
                            <th scope="col">Edit</th>
                            <th scope="col">Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($posts as $post)
                            <tr>
                                <td>
                                    @if (strpos($post->featured_image, 'http') !== false)
                                    <img style="width: 32px;height: 32px" class="front-image" src="{{ $post->featured_image }}" alt="Thumbnail">
                                    @else
                                    <img style="width: 32px;height: 32px" class="front-image" src="/uploads/{{ $post->featured_image }}" alt="Thumbnail">
                                    @endif
                                </td>
                                <th scope="row">{{ $post->title }}</th>
                                <th scope="row">{{ $categories[$post->category] }}</th>
                                <th scope="row">{{ $post->user['name'] }}</th>
                                <td>{{ link_to('posts/' . $post->id . '/edit', $title = 'Edit') }}</td>
                                <td>
                                    {!! Form::open(['route'=>['posts.destroy', $post->id], 'method'=>'DELETE', 'id' => 'form' . $post->id, 'files' => true, 'role' => 'form', 'novalidate' => '']) !!}
                                    <div class="form-group">
                                            {{ Form::submit('Delete', ['class' => 'btn btn-primary']) }}
                                        </div><!-- form-group -->
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div><!-- col-md-8 -->
            </div><!-- row justify-content-center -->
        </div><!-- container -->
    </div><!-- py-4 -->
    
    @endsection