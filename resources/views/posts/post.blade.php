@extends('layouts.site')


@section('content')

<div class="content">
    <h1>{{ $post->title }}</h1>
    @if (strpos($post->featured_image, 'http') !== false)
    <img class="post-image" src="{{ $post->featured_image }}" alt="Thumbnail">
    @else
    <img class="post-image" src="/uploads/{{ $post->featured_image }}" alt="Thumbnail">
    @endif
    <div class="meta">
    <span class="publish-date">{{ date("d-m-Y", strtotime($post->created_at)) }}</span>
    </div><!-- meta -->
    {{ $post->body }}
</div><!-- content -->
@endsection