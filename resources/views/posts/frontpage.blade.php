@extends('layouts.site')

@section('content')
<div class="tile is-ancestor">
    <div class="tile is-vertical is-4 is-parent news-column">
        <h2 class="title">Politics</h2>
        @foreach ($politicnews as $post)
            <div class="news">
                <a href="{{ $post->url() }}"><h2 style="padding-bottom: 15px;" class="subtitle">{{ $post->title }}</h2></a>
                <a href="{{ $post->url() }}">
                    @if (strpos($post->featured_image, 'http') !== false)
                    <img class="front-image" src="{{ $post->featured_image }}" alt="Thumbnail">
                    @else
                    <img class="front-image" src="/uploads/{{ $post->featured_image }}" alt="Thumbnail">
                    @endif
                </a>
            </div><!-- news -->
        @endforeach
    </div><!-- tile is-vertical is-4 is-parent news-column -->
    <div class="tile">
        <div class="tile is-parent is-vertical news-column">
            <h2 class="title">Tech</h2>
                @foreach ($technews as $post)
                <div class="news">
                    <a href="{{ $post->url() }}"><h2 style="padding-bottom: 15px;" class="subtitle">{{ $post->title }}</h2></a>
                    <a href="{{ $post->url() }}">
                        @if (strpos($post->featured_image, 'http') !== false)
                        <img class="front-image" src="{{ $post->featured_image }}" alt="Thumbnail">
                        @else
                        <img class="front-image" src="/uploads/{{ $post->featured_image }}" alt="Thumbnail">
                        @endif
                    </a>
                </div><!-- news -->
                @endforeach
        </div><!-- tile is-parent is-vertical news-column -->
    </div><!-- tile -->
    <div class="tile">
        <div class="tile is-parent is-vertical">
            <h2 class="title">Sport</h2>
            @foreach ($sportnews as $post)
            <div class="news">
                <a href="{{ $post->url() }}"><h2 style="padding-bottom: 15px;" class="subtitle">{{ $post->title }}</h2></a>
                <a href="{{ $post->url() }}">
                    @if (strpos($post->featured_image, 'http') !== false)
                    <img class="front-image" src="{{ $post->featured_image }}" alt="Thumbnail">
                    @else
                    <img class="front-image" src="/uploads/{{ $post->featured_image }}" alt="Thumbnail">
                    @endif
                </a>
            </div><!-- news -->
            @endforeach
        </div><!-- tile is-parent is-vertical -->
    </div><!-- tile -->
</div><!-- tile is-ancestor -->
@endsection