
@if ($errors->any())
<ul>
    @foreach ($errors->all() as $error)
        <li class="help is-danger">{{ $error }}</li>
    @endforeach
</ul>
@endif