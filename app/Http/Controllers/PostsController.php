<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class PostsController extends Controller
{
    public $categories = array(
        'tech'     => 'Tech',
        'sport'    => 'Sport',
        'politics' => 'Politics',
        'science'  => 'Science',
        'business' => 'Business',
    );

    public function __construct()
    {
        $this->middleware('auth', ['except' => ['frontpage', 'post', 'search']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::all();
        return view('posts.index', ['categories' => $this->categories, 'posts' => $posts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create', ['categories' => $this->categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|max:255',
            'body' => 'required',
            'category' => 'required',
            'featured_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $imageName = time().'.'.request()->featured_image->getClientOriginalExtension();
        request()->featured_image->move(public_path('uploads'), $imageName);

        Post::create(array(
            'title' => request('title'),
            'body' => request('body'),
            'category' => request('category'),
            'author' => auth()->id(),
            'featured_image' => $imageName,
        ));
        return redirect('posts')->with('status', 'Post created!');
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        return view('posts.edit', ['categories' => $this->categories, 'post' => $post]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        $request->validate([
            'title' => 'required|max:255',
            'body' => 'required',
            'category' => 'required',
            'featured_image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        if(request('featured_image')){
            $imageName = time().'.'.request('featured_image')->getClientOriginalExtension();
            request('featured_image')->move(public_path('uploads'), $imageName);
            $post->update(array(
                'title' => request('title'),
                'body' => request('body'),
                'category' => request('category'),
                'featured_image' => $imageName,
            ));
        }else {
            $post->update(array(
                'title' => request('title'),
                'body' => request('body'),
                'category' => request('category'),
            ));
        }
        return redirect('posts')->with('status', 'Post updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $post->delete();
        return redirect('posts')->with('status', 'Post deleted!');
    }

    public function frontpage(){
        $latest = Post::all();
        $politicnews = Post::where('category', 'politics')->take(10)->get();
        $technews = Post::where('category', 'tech')->take(10)->get();
        $sportnews = Post::where('category', 'sport')->take(10)->get();
        return view('posts.frontpage', [
            'politicnews' => $politicnews,
            'technews' => $technews,
            'sportnews' => $sportnews
        ]);
    }

    public function search(){
        $search_term = request('search');
        $posts = Post::where('title', 'LIKE', '%' . $search_term . '%')->get();
        return view('posts.search', compact('posts'));
    }

    public function post(Post $post){
        return view('posts.post', compact('post'));
    }
}
