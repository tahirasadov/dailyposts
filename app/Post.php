<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Post extends Model
{

    protected $fillable = ['title', 'slug', 'body', 'featured_image', 'author', 'category'];

    public function user()
    {
        return $this->belongsTo(User::class, 'author');
    }

    public function getRoutrKeyName()
    {
        return 'slug';
    }


    public function url()
    {
        return url("/p/{$this->id}-" . Str::slug($this->title));
    }


}
